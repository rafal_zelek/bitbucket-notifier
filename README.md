### Info ###
Bitbucket notifier is a chrome extension. It notifies you if there are new commits to any of your repositories. And provides shortcuts to your repositories.

### Installing ###
You can install it using the chrome web store:

https://chrome.google.com/webstore/detail/bitbucket-notifier/jncnafmlekinjbdlcncbpclfpmgodcmf

Or by going to *chrome://extensions/* (make sure *Developer mode* is checked). Click *Load unpacked extension...* and select the *chrome/src* folder.